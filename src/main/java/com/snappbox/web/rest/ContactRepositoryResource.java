package com.snappbox.web.rest;

import com.snappbox.domain.ContactRepository;
import com.snappbox.repository.ContactRepositoryRepository;
import com.snappbox.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.snappbox.domain.ContactRepository}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ContactRepositoryResource {

    private final Logger log = LoggerFactory.getLogger(ContactRepositoryResource.class);

    private static final String ENTITY_NAME = "assignContactRepository";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactRepositoryRepository contactRepositoryRepository;

    public ContactRepositoryResource(ContactRepositoryRepository contactRepositoryRepository) {
        this.contactRepositoryRepository = contactRepositoryRepository;
    }

    /**
     * {@code POST  /contact-repositories} : Create a new contactRepository.
     *
     * @param contactRepository the contactRepository to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactRepository, or with status {@code 400 (Bad Request)} if the contactRepository has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-repositories")
    public ResponseEntity<ContactRepository> createContactRepository(@RequestBody ContactRepository contactRepository) throws URISyntaxException {
        log.debug("REST request to save ContactRepository : {}", contactRepository);
        if (contactRepository.getId() != null) {
            throw new BadRequestAlertException("A new contactRepository cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactRepository result = contactRepositoryRepository.save(contactRepository);
        return ResponseEntity.created(new URI("/api/contact-repositories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-repositories} : Updates an existing contactRepository.
     *
     * @param contactRepository the contactRepository to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactRepository,
     * or with status {@code 400 (Bad Request)} if the contactRepository is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactRepository couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-repositories")
    public ResponseEntity<ContactRepository> updateContactRepository(@RequestBody ContactRepository contactRepository) throws URISyntaxException {
        log.debug("REST request to update ContactRepository : {}", contactRepository);
        if (contactRepository.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactRepository result = contactRepositoryRepository.save(contactRepository);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactRepository.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-repositories} : get all the contactRepositories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactRepositories in body.
     */
    @GetMapping("/contact-repositories")
    public List<ContactRepository> getAllContactRepositories() {
        log.debug("REST request to get all ContactRepositories");
        return contactRepositoryRepository.findAll();
    }

    /**
     * {@code GET  /contact-repositories/:id} : get the "id" contactRepository.
     *
     * @param id the id of the contactRepository to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactRepository, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-repositories/{id}")
    public ResponseEntity<ContactRepository> getContactRepository(@PathVariable Long id) {
        log.debug("REST request to get ContactRepository : {}", id);
        Optional<ContactRepository> contactRepository = contactRepositoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(contactRepository);
    }

    /**
     * {@code DELETE  /contact-repositories/:id} : delete the "id" contactRepository.
     *
     * @param id the id of the contactRepository to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-repositories/{id}")
    public ResponseEntity<Void> deleteContactRepository(@PathVariable Long id) {
        log.debug("REST request to delete ContactRepository : {}", id);
        contactRepositoryRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
