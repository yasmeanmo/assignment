package com.snappbox.web.rest;

import com.snappbox.domain.Util;
import com.snappbox.service.ContactQueryService;
import com.snappbox.service.ContactService;
import com.snappbox.service.dto.ContactCriteria;
import com.snappbox.service.dto.ContactDTO;
import com.snappbox.service.dto.SearchContactDTO;
import com.snappbox.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;

/**
 * REST controller for managing {@link com.snappbox.domain.Contact}.
 */
@RestController
@RequestMapping("/api")
public class ContactResource {

    @Autowired
    private BlockingQueue<ContactDTO> queue;

    private final Logger log = LoggerFactory.getLogger(ContactResource.class);

    private static final String ENTITY_NAME = "assignContact";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;


    private final ContactService contactService;

    private final ContactQueryService contactQueryService;

    public ContactResource(ContactService contactService, ContactQueryService contactQueryService) {
        this.contactService = contactService;
        this.contactQueryService = contactQueryService;
    }

    /**
     * {@code POST  /searchContacts} : search for a specific contact.
     *
     * @param contactDTO the contactDTO to search.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body that include list of contactDTO, or with status {@code 400 (Bad Request)}
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/searchContacts")
    public ResponseEntity<Object> createContact(@Valid @RequestBody SearchContactDTO contactDTO) throws URISyntaxException {
        log.debug("REST request to search Contact : {}", contactDTO );
        List<ContactDTO> result = contactQueryService.findByCriteria(Util.contactCriteriaBuilder(contactDTO));
        if(result.size() == 0){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No record matched criteria");
        }
        return ResponseEntity.ok()
            .headers(HeaderUtil.createAlert(applicationName, "searching with criteria", ENTITY_NAME))
            .body(result);
    }

    /**
     * {@code PUT  /addContacts} : Add a contact.
     *
     * @param contactDTO the contactDTO to add.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactDTO,
     * or with status {@code 400 (Bad Request)} if the contactDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactDTO couldn't be added.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/addContacts")
    public ResponseEntity<Object> addContact(@Valid @RequestBody ContactDTO contactDTO) throws URISyntaxException {
        log.debug("REST request to update Contact : {}", contactDTO);
        if (contactDTO.getId() != null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        contactDTO.setValid(true);
        contactDTO.setGrabbed(false);
        try {
            ContactDTO result = contactService.save(contactDTO);
            queue.add(result);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
        }catch (DataIntegrityViolationException e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Record already exist");
        }

    }

    /**
     * {@code GET  /contacts} : get all the contacts.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contacts in body.
     */
    @GetMapping("/contacts")
    public ResponseEntity<List<ContactDTO>> getAllContacts(ContactCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Contacts by criteria: {}", criteria);
        Page<ContactDTO> page = contactQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contacts/:id} : get the "id" contact.
     *
     * @param id the id of the contactDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contacts/{id}")
    public ResponseEntity<ContactDTO> getContact(@PathVariable Long id) {
        log.debug("REST request to get Contact : {}", id);
        Optional<ContactDTO> contactDTO = contactService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactDTO);
    }

}
