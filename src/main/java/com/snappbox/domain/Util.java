package com.snappbox.domain;

import com.snappbox.service.dto.ContactCriteria;
import com.snappbox.service.dto.SearchContactDTO;
import io.github.jhipster.service.filter.StringFilter;

public class Util {
    /**
     * This method is building ContactCriteria for specify desire query
     *
     * @param contactDTO as input
     * @return contactCriteria
     * @see com.snappbox.web.rest.ContactResource;
     * @since 0.0.1
     */
    public static ContactCriteria contactCriteriaBuilder(SearchContactDTO contactDTO){
        ContactCriteria contactCriteria = new ContactCriteria();
        if (contactDTO.getName() != null) {
            contactCriteria.setName( new StringFilter().setContains(contactDTO.getName()));
        }
        if (contactDTO.getEmail() != null) {
            contactCriteria.setEmail( new StringFilter().setContains(contactDTO.getEmail()));
        }
        if (contactDTO.getPhoneNumber() != null) {
            contactCriteria.setPhoneNumber( new StringFilter().setContains(contactDTO.getPhoneNumber()));
        }
        if (contactDTO.getGithubAccount() != null) {
            contactCriteria.setGitHubRepositories( new StringFilter().setContains(contactDTO.getGithubAccount()));
        }
        if (contactDTO.getOrganization() != null) {
            contactCriteria.setOrganization( new StringFilter().setContains(contactDTO.getOrganization()));
        }
        return contactCriteria;
    }

}
