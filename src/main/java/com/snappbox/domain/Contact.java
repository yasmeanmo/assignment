package com.snappbox.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Contact.
 */
@Entity
@Table(name = "contact" ,  uniqueConstraints= @UniqueConstraint(columnNames={"name", "phone_number"}))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Za-z]+", message = "invalid format for name")
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Pattern(regexp = "^09[0-9]{9}" , message = "invalid phoneNumber format")
    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

    @Column(name = "organization")
    private String organization;

    @Pattern(regexp = "^[0-9A-Za-z]+@[A-Za-z]+\\.[A-Za-z]+" , message = "invalid email format" )
    @Column(name = "email")
    private String email;

    @Column(name = "github_account")
    private String githubAccount;

    @NotNull
    @Column(name = "is_grabbed", nullable = false)
    private Boolean isGrabbed;

    @NotNull
    @Column(name = "is_valid", nullable = false)
    private Boolean isValid;

    @OneToMany(mappedBy = "contact")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ContactRepository> contactRepositories = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Contact name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Contact phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOrganization() {
        return organization;
    }

    public Contact organization(String organization) {
        this.organization = organization;
        return this;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getEmail() {
        return email;
    }

    public Contact email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGithubAccount() {
        return githubAccount;
    }

    public Contact githubAccount(String githubAccount) {
        this.githubAccount = githubAccount;
        return this;
    }

    public void setGithubAccount(String githubAccount) {
        this.githubAccount = githubAccount;
    }

    public Boolean isIsGrabbed() {
        return isGrabbed;
    }

    public Contact isGrabbed(Boolean isGrabbed) {
        this.isGrabbed = isGrabbed;
        return this;
    }

    public void setIsGrabbed(Boolean isGrabbed) {
        this.isGrabbed = isGrabbed;
    }

    public Boolean isIsValid() {
        return isValid;
    }

    public Contact isValid(Boolean isValid) {
        this.isValid = isValid;
        return this;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }


    public Set<ContactRepository> getContactRepositories() {
        return contactRepositories;
    }

    public Contact contactRepositories(Set<ContactRepository> contactRepositories) {
        this.contactRepositories = contactRepositories;
        return this;
    }

    public Contact addContactRepository(ContactRepository contactRepository) {
        this.contactRepositories.add(contactRepository);
        contactRepository.setContact(this);
        return this;
    }

    public Contact removeContactRepository(ContactRepository contactRepository) {
        this.contactRepositories.remove(contactRepository);
        contactRepository.setContact(null);
        return this;
    }

    public void setContactRepositories(Set<ContactRepository> contactRepositories) {
        this.contactRepositories = contactRepositories;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Contact)) {
            return false;
        }
        return id != null && id.equals(((Contact) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Contact{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", organization='" + getOrganization() + "'" +
            ", email='" + getEmail() + "'" +
            ", githubAccount='" + getGithubAccount() + "'" +
            ", isGrabbed='" + isIsGrabbed() + "'" +
            ", isValid='" + isIsValid() + "'" +
            "}";
    }
}
