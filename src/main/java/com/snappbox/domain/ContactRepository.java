package com.snappbox.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ContactRepository.
 */
@Entity
@Table(name = "contact_repository")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ContactRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "repository")
    private String repository;

    @ManyToOne
    @JsonIgnoreProperties(value = "contactRepositories", allowSetters = true)
    private Contact contact;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRepository() {
        return repository;
    }

    public ContactRepository repository(String repository) {
        this.repository = repository;
        return this;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public Contact getContact() {
        return contact;
    }

    public ContactRepository contact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactRepository)) {
            return false;
        }
        return id != null && id.equals(((ContactRepository) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactRepository{" +
            "id=" + getId() +
            ", repository='" + getRepository() + "'" +
            "}";
    }
}
