package com.snappbox.service.impl;

import com.snappbox.domain.Contact;
import com.snappbox.repository.ContactRepository;
import com.snappbox.repository.ContactRepositoryRepository;
import com.snappbox.service.ContactService;
import com.snappbox.service.dto.ContactDTO;
import com.snappbox.service.mapper.ContactLazyMapper;
import com.snappbox.service.mapper.ContactMapper;
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Contact}.
 */
@Service
@Transactional
public class ContactServiceImpl implements ContactService {

    private final BlockingQueue<ContactDTO> queue;

    private final Logger log = LoggerFactory.getLogger(ContactServiceImpl.class);

    private final ContactRepository contactRepository;

    @Autowired
    private ContactRepositoryRepository contactRepositoryRepository;

    private final ContactMapper contactMapper;

    private final ContactLazyMapper contactLazyMapper;

    private RestTemplate restTemplate = new RestTemplate();

    public ContactServiceImpl(ContactRepository contactRepository, ContactMapper contactMapper,BlockingQueue<ContactDTO> queue, ContactLazyMapper contactLazyMapper) {
        this.contactRepository = contactRepository;
        this.contactMapper = contactMapper;
        this.queue=queue;
        this.contactLazyMapper= contactLazyMapper;
    }

    @Override
    public ContactDTO save(ContactDTO contactDTO) {
        log.debug("Request to save Contact : {}", contactDTO);
        Contact contact = contactMapper.toEntity(contactDTO);
        contact = contactRepository.save(contact);
        return contactMapper.toDto(contact);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ContactDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Contacts");
        return contactRepository.findAll(pageable)
            .map(contactMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ContactDTO> findOne(Long id) {
        log.debug("Request to get Contact : {}", id);
        return contactRepository.findById(id)
            .map(contactMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Contact : {}", id);
        contactRepository.deleteById(id);
    }

    @Override
    public List<ContactDTO> findAllNonGrabbed() {
        log.debug("Request to find all contacts which repository is empty");
        return contactRepository.findAllNonGrabbed().stream()
            .map(contactLazyMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
//
    @PostConstruct
    private void initialLoadIntoQueue(){
        queue.addAll(findAllNonGrabbed());
    }

    /**
     * This method is Scheduled job which send request to gitHubAPI to get name of the repositories
     * and save them.
     * @since 0.0.1
     */

    @Scheduled(fixedDelay = 30000)
    public void fetchGitHubRepositoryJob() {
        ContactDTO contactDTO = queue.poll();
        log.debug("Job Started for Github Data Grabbing for Contact {}",contactDTO);
        if( contactDTO != null) {
            ResponseEntity<JSONArray> responseEntity = null;
            try {
                responseEntity = restTemplate.getForEntity(generateUrl(contactDTO), JSONArray.class);
                for (Object repoObject : Objects.requireNonNull(responseEntity.getBody()).toArray()) {
                    contactDTO.getContactRepositories().add(contactRepositoryRepository
                        .save(new com.snappbox.domain.ContactRepository()
                        .repository(((LinkedHashMap<String,String>) repoObject).get("name")).contact(contactMapper.toEntity(contactDTO))));
                }
                contactDTO.setValid(true);
                contactDTO.setGrabbed(true);
                save(contactDTO);
            }catch (HttpClientErrorException ex) {
                if (ex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                    contactDTO.setValid(false);
                    contactDTO.setGrabbed(true);
                    save(contactDTO);
                } else
                    queue.add(contactDTO);
            }
        }
        log.debug("Execute method asynchronously. " + Thread.currentThread().getName());
    }

    private String generateUrl(ContactDTO contactDTO){
        return "https://api.github.com/users/"+contactDTO.getGithubAccount()+"/repos";
    }
}
