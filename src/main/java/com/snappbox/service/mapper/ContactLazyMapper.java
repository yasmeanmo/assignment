package com.snappbox.service.mapper;


import com.snappbox.domain.Contact;
import com.snappbox.service.dto.ContactDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Contact} and its DTO {@link ContactDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ContactLazyMapper extends EntityMapper<ContactDTO, Contact> {


    @Mapping(target = "contactRepositories", ignore = true)
    @Mapping(target = "removeContactRepository", ignore = true)
    Contact toEntity(ContactDTO contactDTO);

//    ContactDTO toDto(Contact contact);

    @Mapping(target = "contactRepositories", ignore = true)
    ContactDTO toDto(Contact contact);

    default Contact fromId(Long id) {
        if (id == null) {
            return null;
        }
        Contact contact = new Contact();
        contact.setId(id);
        return contact;
    }
}
