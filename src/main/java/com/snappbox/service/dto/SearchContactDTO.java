package com.snappbox.service.dto;

import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Objects;

public class SearchContactDTO implements Serializable {
    @Pattern(regexp = "^[A-Za-z]+")
    private String name;

    @Pattern(regexp = "^09[0-9]{9}")
    private String phoneNumber;

    private String organization;

    @Pattern(regexp = "^[0-9A-Za-z]+@[A-Za-z]+\\.[A-Za-z]+")
    private String email;

    private String githubAccount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGithubAccount() {
        return githubAccount;
    }

    public void setGithubAccount(String githubAccount) {
        this.githubAccount = githubAccount;
    }


    @Override
    public int hashCode() {
        return Objects.hash(name, phoneNumber, organization, email, githubAccount);
    }

    @Override
    public String toString() {
        return "SearchContactDTO{" +
            "name='" + name + '\'' +
            ", phoneNumber='" + phoneNumber + '\'' +
            ", organization='" + organization + '\'' +
            ", email='" + email + '\'' +
            ", githubAccount='" + githubAccount + '\'' +
            '}';
    }
}
