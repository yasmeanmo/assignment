package com.snappbox.service.dto;

import com.snappbox.domain.ContactRepository;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link com.snappbox.domain.Contact} entity.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactDTO implements Serializable {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Za-z]+")
    private String name;

    @NotNull
    @Pattern(regexp = "^09[0-9]{9}")
    private String phoneNumber;

    private String organization;

    @Pattern(regexp = "^[0-9A-Za-z]+@[A-Za-z]+\\.[A-Za-z]+")
    private String email;

    private String githubAccount;

    private Boolean isGrabbed;

    private Boolean isValid;

    private final Set<ContactRepository> contactRepositories = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGithubAccount() {
        return githubAccount;
    }

    public void setGithubAccount(String githubAccount) {
        this.githubAccount = githubAccount;
    }

    public Boolean isIsGrabbed() {
        return isGrabbed;
    }

    public void setIsGrabbed(Boolean isGrabbed) {
        this.isGrabbed = isGrabbed;
    }

    public Boolean isIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public void setGrabbed(Boolean grabbed) {
        isGrabbed = grabbed;
    }


    public void setValid(Boolean valid) {
        isValid = valid;
    }

    public Set<ContactRepository> getContactRepositories() {
        return contactRepositories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", organization='" + getOrganization() + "'" +
            ", email='" + getEmail() + "'" +
            ", githubAccount='" + getGithubAccount() + "'" +
            ", isGrabbed='" + isIsGrabbed() + "'" +
            ", isValid='" + isIsValid() + "'" +
            ", gitHubRepositories='" + contactRepositories.toString() + "'" +
            "}";
    }
}
