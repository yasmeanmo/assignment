package com.snappbox.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.snappbox.domain.Contact} entity. This class is used
 * in {@link com.snappbox.web.rest.ContactResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contacts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter phoneNumber;

    private StringFilter organization;

    private StringFilter email;

    private StringFilter githubAccount;

    private BooleanFilter isGrabbed;

    private BooleanFilter isValid;

    private StringFilter gitHubRepositories;

    private LongFilter contactRepositoryId;

    public ContactCriteria() {
    }

    public ContactCriteria(ContactCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.phoneNumber = other.phoneNumber == null ? null : other.phoneNumber.copy();
        this.organization = other.organization == null ? null : other.organization.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.githubAccount = other.githubAccount == null ? null : other.githubAccount.copy();
        this.isGrabbed = other.isGrabbed == null ? null : other.isGrabbed.copy();
        this.isValid = other.isValid == null ? null : other.isValid.copy();
        this.gitHubRepositories = other.gitHubRepositories == null ? null : other.gitHubRepositories.copy();
        this.contactRepositoryId = other.contactRepositoryId == null ? null : other.contactRepositoryId.copy();
    }


    @Override
    public ContactCriteria copy() {
        return new ContactCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(StringFilter phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public StringFilter getOrganization() {
        return organization;
    }

    public void setOrganization(StringFilter organization) {
        this.organization = organization;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getGithubAccount() {
        return githubAccount;
    }

    public void setGithubAccount(StringFilter githubAccount) {
        this.githubAccount = githubAccount;
    }

    public BooleanFilter getIsGrabbed() {
        return isGrabbed;
    }

    public void setIsGrabbed(BooleanFilter isGrabbed) {
        this.isGrabbed = isGrabbed;
    }

    public BooleanFilter getIsValid() {
        return isValid;
    }

    public void setIsValid(BooleanFilter isValid) {
        this.isValid = isValid;
    }

    public StringFilter getGitHubRepositories() {
        return gitHubRepositories;
    }

    public void setGitHubRepositories(StringFilter gitHubRepositories) {
        this.gitHubRepositories = gitHubRepositories;
    }

    public LongFilter getContactRepositoryId() {
        return contactRepositoryId;
    }

    public void setContactRepositoryId(LongFilter contactRepositoryId) {
        this.contactRepositoryId = contactRepositoryId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactCriteria that = (ContactCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(phoneNumber, that.phoneNumber) &&
            Objects.equals(organization, that.organization) &&
            Objects.equals(email, that.email) &&
            Objects.equals(githubAccount, that.githubAccount) &&
            Objects.equals(isGrabbed, that.isGrabbed) &&
            Objects.equals(isValid, that.isValid) &&
            Objects.equals(gitHubRepositories, that.gitHubRepositories) &&
            Objects.equals(contactRepositoryId, that.contactRepositoryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        phoneNumber,
        organization,
        email,
        githubAccount,
        isGrabbed,
        isValid,
        gitHubRepositories,
        contactRepositoryId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (phoneNumber != null ? "phoneNumber=" + phoneNumber + ", " : "") +
                (organization != null ? "organization=" + organization + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (githubAccount != null ? "githubAccount=" + githubAccount + ", " : "") +
                (isGrabbed != null ? "isGrabbed=" + isGrabbed + ", " : "") +
                (isValid != null ? "isValid=" + isValid + ", " : "") +
                (gitHubRepositories != null ? "gitHubRepositories=" + gitHubRepositories + ", " : "") +
                (contactRepositoryId != null ? "contactRepositoryId=" + contactRepositoryId + ", " : "") +
            "}";
    }

}
