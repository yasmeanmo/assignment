package com.snappbox.config;

import com.snappbox.service.dto.ContactDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

@Configuration
public class QueueConfiguration {

    private final BlockingQueue <ContactDTO> queue = new LinkedBlockingDeque<>();

    @Bean
    public BlockingQueue<ContactDTO> queue(){
        return queue;
    }

}
