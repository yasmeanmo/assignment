package com.snappbox.repository;

import com.snappbox.domain.Contact;
import com.snappbox.domain.ContactRepository;

import com.snappbox.service.dto.ContactDTO;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the ContactRepository entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactRepositoryRepository extends JpaRepository<ContactRepository, Long> {

}
