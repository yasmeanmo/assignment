package com.snappbox.repository;

import com.snappbox.domain.Contact;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Contact entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactRepository extends JpaRepository<Contact, Long>, JpaSpecificationExecutor<Contact> {
    @Query("SELECT c FROM Contact c where c.isGrabbed = false and c.isValid = true")
    public List<Contact> findAllNonGrabbed();
}
