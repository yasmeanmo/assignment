package com.snappbox.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.snappbox.web.rest.TestUtil;

public class ContactRepositoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactRepository.class);
        ContactRepository contactRepository1 = new ContactRepository();
        contactRepository1.setId(1L);
        ContactRepository contactRepository2 = new ContactRepository();
        contactRepository2.setId(contactRepository1.getId());
        assertThat(contactRepository1).isEqualTo(contactRepository2);
        contactRepository2.setId(2L);
        assertThat(contactRepository1).isNotEqualTo(contactRepository2);
        contactRepository1.setId(null);
        assertThat(contactRepository1).isNotEqualTo(contactRepository2);
    }
}
