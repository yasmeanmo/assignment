package com.snappbox.web.rest;

import com.snappbox.AssignApp;
import com.snappbox.service.dto.ContactDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.Validator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = { AssignApp.class, Validator.class})
@AutoConfigureMockMvc
public class AddContactTest {

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
    @Autowired
    private Validator validator;
    @Autowired
    private MockMvc mockMvc;



    @Test
    @Transactional
    public void addContactTest() throws Exception {
        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setName("yas");
        contactDTO.setPhoneNumber("09129857493");

        HttpHeaders httpHeaders = new HttpHeaders();

        mockMvc.perform(put("/api/addContacts")
            .headers(httpHeaders)
//            .contentType(TestUtil.)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isOk());
    }
}
